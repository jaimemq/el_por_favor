package el_por_favor.cheetahchat;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**Class Name: MainMenu
 * Extends: AppCompatActivity
 * Implements: OnClickListener
 * Resume: This is the main menu of our application. Button to Chat, and selection mode buttons.
 * The WiFiDirect connection is established in this class. All method related with WifiDirect connection
 * here
 * Multi-connection help:
 * http://stackoverflow.com/questions/22732835/multiple-clients-access-the-server-concurrently
 *
 *
 * @author Jaime Moreno Quintanar (999132)
 * @version 8.11.2016*/
public class MainMenu extends AppCompatActivity implements View.OnClickListener {

    ImageButton buttonToChat;


    //Am I client?
    public static boolean client;

    // UI elements
    Button btnClientMode;
    Button btnServerMode;
    TextView txtModeSelection;
    TextView linetext;



    private boolean isWifiP2pEnabled = false;



    //Elements declaration
    Toolbar toolbar;


    // What mode are we in?
    boolean mInClientMode;

    // Are we connected?
    boolean mIsConnected = false;


    // WiFi management
    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;


    /**
     *onCreate
     * @param savedInstanceState savedInstanceState
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //Element initiation
        buttonToChat = (ImageButton) findViewById(R.id.buttonToChat);
        buttonToChat.setOnClickListener(this);

        setupUI();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



    }

    /**
     * onClick
     * With buttonToChat we go to Chat activity
     * With btn_client_mode we select Client mode. The device start to find peers
     * With btn_server_mode we select Server mode. The device start to find peers
     *
     * When peers are found, the WiFiDirect connection is established
     *
     *
     * @param v view
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonToChat:
                Log.i("MainMenu", "You are going to Chat activity)");

                Intent intentChat = new Intent(this, Chat.class);
                startActivity(intentChat);
                break;
            case R.id.btn_client_mode:
                Log.i("MainMenu", "You are CLIENT");
                client = true;
                Log.i("MainMenu", "You are CLIENT. Going to setupWifiDirect(boolean)");

                Toast.makeText(this, "Client Mode ON", Toast.LENGTH_SHORT).show();
                this.mInClientMode = true;
                setupWifiDirect(false);
                Log.i("MainMenu", "Coming from setupServer(). Going to discoverPeers()");

                discoverPeers();


                break;
            case R.id.btn_server_mode:
                Log.i("MainMenu", "You are SERVER");
                client = false;
                Log.i("MainMenu", "You are SERVER. Going to setupServer()");

                Toast.makeText(this, "Server Mode ON", Toast.LENGTH_SHORT).show();

                // hidePrompt("Server Mode");
                setupServer();
                break;
            default:
                break;
        }
    }


    /**
     * Method to create the popup menu
     * @param menu menu
     * @return boolean
     */



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Method to show action_settings when the popup menu button is pressed
     * @param item menuitem
     * @return int
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





/*
    @Override
    protected void onPause() {
        super.onPause();
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }*/

    /**
     *
     */
    @Override
    protected void onDestroy() {
      //  shutdownCommunication();
        super.onDestroy();
    }


    /**
     * Set up of the UI elements in MainMenu
     */
    private void setupUI() {
        Log.i("MainMenu", "You are in setupUI()");
        btnClientMode = (Button) findViewById(R.id.btn_client_mode);
        btnServerMode = (Button) findViewById(R.id.btn_server_mode);
        btnClientMode.setOnClickListener(this);
        btnServerMode.setOnClickListener(this);
        txtModeSelection = (TextView) findViewById(R.id.prompt);
        linetext = (TextView) findViewById(R.id.line);
    }




    /**
     * Called when new P2P devices become available
     * @param devices devices connected
     */
    public void onAvailableP2pDevices(final WifiP2pDevice[] devices) {
        Log.i("MainMenu", "You are in onAvailableP2pDevice().");

        Toast.makeText(MainMenu.this,"Number of peers discovered: " + devices.length , Toast.LENGTH_SHORT).show();

        Log.i("MainMenu", "onAvailableP2pDevice(). Number of peers discovered: " + devices.length);
        if (mIsConnected) {
            Log.i("MainMenu", "You are in onAvailableP2pDevice(). Devices ARE CONNECTED. DO NOTHING");

            // if we are already connected, we do nothing
            return;
        }

        if (devices.length < 1) {
            Log.i("MainMenu", "You are in onAvailableP2pDevice(). Devices length < 1");
            // sanity check
            return;
        }

        if (mInClientMode) {
            // let us try to connect to the server
            Log.i("MainMenu", "You are in onAvailableP2pDevice(). You are CLIENT so");

            new Thread() {
                @Override
                public void run() {
                    Log.i("MainMenu", "You are in onAvailableP2pDevice(). Inside thread to connect with Server" +
                            "Go to connectToServer");

                    connectToServer(devices[0]);
                }
            }.start();
        }
    }

    /**
     * Set up of the WifiDirect
     * @param isServer server?
     */
    private void setupWifiDirect(final boolean isServer) {
        Log.i("MainMenu", "You are in setupWifiDirect(boolean)");

        // necessary setup for P2P mode
        mIntentFilter = new IntentFilter();
        //  Indicates a change in the Wi-Fi Peer-to-Peer status.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        // Indicates a change in the list of available peers.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        // Indicates the state of Wi-Fi P2P connectivity has changed.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        // Indicates this device's details have changed.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        WifiP2pConfig config = new WifiP2pConfig();
        if (isServer) {
            Log.i("MainMenu", "You are in setupServer(). As you are server, you are GroupOwner");

            config.groupOwnerIntent = 15;
        }

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
    }

    /**
     * Method to discover peers. If failure, discoverPeers is called until success
     */
    private void discoverPeers() {
        Log.i("MainMenu", "You are in discoverPeers()");

        //sleep3000();
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Log.i("MainMenu", "You are in discoverPeers()inside WifiP2pManager.ActionListener. " +
                        "SUCCESS!");

                Toast.makeText(MainMenu.this, "Success: FOUND PEERS", Toast.LENGTH_SHORT).show();
                mManager.requestPeers(mChannel, new WifiP2pManager.PeerListListener() {

                    @Override
                    public void onPeersAvailable(WifiP2pDeviceList peers) {
                        Log.i("Chat", "You are in discoverPeers()inside onPeersAvailable(WifiP2pDeviceList)." +
                                "You call onAvailableP2pDevices");

                        MainMenu.this.onAvailableP2pDevices(
                                peers.getDeviceList().toArray(new WifiP2pDevice[peers.getDeviceList().size()]));
                    }
                });
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.i("MainMenu", "You are in discoverPeers()inside WifiP2pManager.ActionListener. " +
                        "FAILURE!");
                Toast.makeText(MainMenu.this, "Failure: NO PEERS", Toast.LENGTH_SHORT).show();
                discoverPeers();
            }
        });
    }


    /**SERVER
     * Setup of the server: WifiDirect settings and discover peers
     */
    private void setupServer() {
        Log.i("MainMenu", "You are in setupServer(). Going to setupWifiDirect(boolean)");

        setupWifiDirect(true);
        //Log.i("Chat", "You are in setupServer(). Coming from setupWifiDirect(boolean). Going to discoverPeers()");
        Log.i("MainMenu", "You are in setupServer(). Coming from setupWifiDirect. Go to discoverPeers");
        discoverPeers();

    }

    /**
     * CLIENT
     * Connect to the server
     * @param device the server device
     *
     */
    private void connectToServer(WifiP2pDevice device) {
        Log.i("MainMenu", "connectToServer(WifiP2pDevice).Connecting to server");
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.groupOwnerIntent = 0;
        config.wps.setup = WpsInfo.PBC;
        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                mManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                    @Override
                    public void onConnectionInfoAvailable(final WifiP2pInfo wifiP2pInfo) {

                    }
                });
            }

            @Override
            public void onFailure(int reason) {
                //failure logic
            }
        });
    }

}

