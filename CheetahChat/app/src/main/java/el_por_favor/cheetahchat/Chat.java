package el_por_favor.cheetahchat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**Class Name: Chat
 * Extends: AppCompatActivity
 * Implements: OnClickListener
 * Resume: All functionalities related with UI of the chat will be developed on this activity
 * Each time user sends a message, it will be shown on the screen inside a conversation cloud
 *
 * Socket connection between server and clients here
 *
 * ListView:
 * https://www.youtube.com/watch?v=v7fULw96rC4&t=192s
 *
 * Error: E/SpannableStringBuilder: SPAN_EXCLUSIVE_EXCLUSIVE spans cannot have a zero length
 * If use SwiftKey!! Change to Android keyboard and the error disappears
 * Error2: If the user write more than 11 lines, the sender button disappears from the screen
 * Maybe I can adjust 9.patch bubble chat
 * Error3: E/Surface: getSlotFromBufferLocked: unknown buffer: 0x7f82779220
 * I don't know how to solve it, I think it's a problem only with Marshmallow
 * Any of the errors avoid the app runs properly
 *
 * @author Jaime Moreno Quintanar (999132), Andrés Escalante (998917)
 * @version 8.12.2016*/



public class Chat extends AppCompatActivity implements View.OnClickListener{


    private boolean isWifiP2pEnabled = false;

    public static final int SERVER = 1;
    public static final int CLIENT = 2;
    int whatIAm;

    //Elements declaration - Chat
    Toolbar toolbar;
    ImageButton sendMessageButton;
    EditText messageToSend;
    TextView sentMessage;
    ImageButton backArrow;

    //ListView elements declaration
    ListView listMessages;
    ArrayAdapter<ChatMessage> listAdapter;
    ArrayList<ChatMessage> list;
    boolean isMine = true;


    //SQLiteDatabase
    DatabaseHelper userMessagesDb;
    ImageButton settingsButton;



    public final static int PORT = 9000;
    // What mode are we in?
    boolean mInClientMode;
    // Are we connected?
    boolean mIsConnected = false;


    // WiFi management
    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;


    // Socket connectivity
    Socket mSocket;
    OutputStreamWriter mOutput;
    BufferedReader mInput;

    // server socket
    private ServerSocket serverSocket;


    //Related with messages
    static boolean stop = false;
    String line = null;
    String line2 = null;

    static boolean messageReady = false;
    String message;
    int counter =0;
    boolean sendToClient1 = false;
    boolean sendToClient2 = false;

    String ClientName;

    boolean messageClient1Received = false;
    boolean messageClient2Received = false;
    String buffer;
    String buffer2;



    /*
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //set up
        //SERVER
        if(!MainMenu.client){
            whatIAm = SERVER;
            Log.i("Chat", "You are SERVER. Going to setupServer()");

            Toast.makeText(this, "Server Mode ON", Toast.LENGTH_SHORT).show();
            this.mInClientMode = false;
           // hidePrompt("Server Mode");
            setupServer();
        }
        //CLIENT
        if(MainMenu.client){
            whatIAm = CLIENT;
            Log.i("Chat", "You are CLIENT. Going to setupWifiDirect(boolean)");

            Toast.makeText(this, "Client Mode ON", Toast.LENGTH_SHORT).show();
            this.mInClientMode = true;
            // hidePrompt("Client Mode");
            setupWifiDirect(false);
            Log.i("Chat", "Coming from setupServer(). Going to discoverPeers()");

            discoverPeers();



        }


        //Element initiation
        sendMessageButton = (ImageButton) findViewById(R.id.sendMessageArrow);
        messageToSend = (EditText)findViewById(R.id.writeMessage);
        sentMessage = (TextView)findViewById(R.id.sentMessage);
        backArrow = (ImageButton) findViewById(R.id.backArrow);
        backArrow.setOnClickListener(this);

        listMessages = (ListView) findViewById(R.id.listMessages);
        list = new ArrayList<>();

        //Set ListView adapter
        listAdapter = new MessageAdapter(this, R.layout.message_left, list);
        listMessages.setAdapter(listAdapter);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //SQLite
        userMessagesDb = new DatabaseHelper(this);


        settingsButton = (ImageButton) findViewById(R.id.settingsChat);

        /**settings.setOnClickListener
         * Show the data stored in the database
         *
         * @author Jaime Moreno Quintanar (999132)
         * @version 24.11.2016
         * */
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor res = userMessagesDb.getAllData();
                if(res.getCount() == 0){
                    showMessage("Error", "Nothing found");
                }

                StringBuffer buffer = new StringBuffer();
                while (res.moveToNext()){
                    buffer.append("Id :"+res.getString(0)+"\n");
                    buffer.append("User :"+res.getString(1)+"\n");
                    buffer.append("Message :"+res.getString(2)+"\n\n");
                }
                showMessage("Data", buffer.toString());

            }
        });


        /**sendMessageButton.setOnClickListener
        * With this method, when the user press sendMessageButton (arrow to send messages),
        * the message is include on the ListView inside a yellow bubble and then the EditText
        * is cleaned. In case the user try to send an empty message, an information toast is shown
        * asking for entering a message
        *
        * @author Jaime Moreno Quintanar (999132)
        * @version 21.11.2016
        * */
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Answer from StackOverFlow: http://stackoverflow.com/questions/4396376/how-to-get-edittext-value-and-store-it-on-textview

                    if (messageToSend.getText().toString().trim().equals("")) {
                         Toast.makeText(getApplicationContext(), "Please enter a message", Toast.LENGTH_LONG).show();
                    }else {
                        ChatMessage contentSentMessage;
                        //I'm sending a message
                        if (whatIAm == SERVER) {
                            contentSentMessage = new ChatMessage(messageToSend.getText().toString(), isMine, 1, 1); //gets the message
                        }else{
                            contentSentMessage = new ChatMessage(messageToSend.getText().toString(), isMine, 1, 1); //gets the message

                        }
                        list.add(contentSentMessage);
                        listAdapter.notifyDataSetChanged();
                        //insert message in database
                        if(MainMenu.client){
                            userMessagesDb.insertData(ClientName, contentSentMessage.getContent());
                        }
                        if(!MainMenu.client){
                            userMessagesDb.insertData("Server", contentSentMessage.getContent());
                        }

                        messageToSend.setText(""); //After using it, we clean messageToSend


                        //client
                        if (MainMenu.client){
                            sendClientMessage(contentSentMessage.getContent());
                        }
                        //server
                        if(!MainMenu.client){
                            messageReady =true;
                            message = contentSentMessage.getContent();

                        }


                    }

            }
        });


        /**
         * This is the solution to our problem reading messages in real time. In this way unblock
         * readline()
         */
         new  Thread() {

            public void run() {
                while(!stop) {
                    try {
                        if (MainMenu.client) {

                            sleep(2000);

                            mOutput.write("Message that cannot be read\n");
                            mOutput.flush();

                        }
                    } catch (InterruptedException | IOException e) {
                        e.printStackTrace();
                    } catch (NullPointerException | AndroidRuntimeException ignored) {
                    }
                }

            }


        }.start();

    }



        /**OnClick
         * Back to main menu when user press backArrow
         *
         * Jaime Moreno Quintanar (999132)
         * 22.11.2016
         * */
    @Override
    public void onClick(View view) {
        switch (view.getId()){


            case R.id.backArrow:
                Intent intentMainMenu = new Intent(this, MainMenu.class);
                startActivity(intentMainMenu);
                break;
            default:
                break;
        }
    }

    /**
     * To show the list of messages stored in database
     * @param title title of the message
     * @param message message
     */
    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }



    /**
     * Called when new P2P devices become available
     *
     * Code taken from SmartColor(Mariusz)
     * @param devices list of devices we have
     */
    public void onAvailableP2pDevices(final WifiP2pDevice[] devices) {
        Log.i("Chat", "You are in onAvailableP2pDevice().");

        Log.i("Chat", "onAvailableP2pDevice(). Number of peers discovered: " + devices.length);
        if (mIsConnected) {
            Log.i("Chat", "You are in onAvailableP2pDevice(). Devices ARE CONNECTED. DO NOTHING");

            // if we are already connected, we do nothing
            return;
        }

        if (devices.length < 1) {
            Log.i("Chat", "You are in onAvailableP2pDevice(). Devices length < 1");
            // sanity check
            return;
        }


        if (mInClientMode) {
            // let us try to connect to the server
            Log.i("Chat", "You are in onAvailableP2pDevice(). You are CLIENT so");

            new Thread() {
                @Override
                public void run() {
                    Log.i("Chat", "You are in onAvailableP2pDevice(). Inside thread to connect with Server" +
                            "Go to connectToServer");

                    connectToServer(devices[0]);
                }
            }.start();
        }
    }

    /**
     * WifiDirect setup. If server, group owner
     * Taken from SmartColor
     * @param isServer Is Server?
     */
    private void setupWifiDirect(final boolean isServer) {
        Log.i("Chat", "You are in setupWifiDirect(boolean)");

        // necessary setup for P2P mode
        mIntentFilter = new IntentFilter();
        //  Indicates a change in the Wi-Fi Peer-to-Peer status.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        // Indicates a change in the list of available peers.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        // Indicates the state of Wi-Fi P2P connectivity has changed.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        // Indicates this device's details have changed.
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        WifiP2pConfig config = new WifiP2pConfig();
        if (isServer) {
            Log.i("Chat", "You are in setupServer(). As you are server, you are GO");

            config.groupOwnerIntent = 15;
        }

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
    }

    /**
     * Method to discover peers
     * Taken from Smart Color
     */
    private void discoverPeers() {
        Log.i("Chat", "You are in discoverPeers()");

        //sleep3000();
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Log.i("Chat", "You are in discoverPeers()inside WifiP2pManager.ActionListener. " +
                        "SUCCESS!");

                Toast.makeText(Chat.this, "Success: FOUND PEERS", Toast.LENGTH_SHORT).show();
                mManager.requestPeers(mChannel, new WifiP2pManager.PeerListListener() {

                    @Override
                    public void onPeersAvailable(WifiP2pDeviceList peers) {
                        Log.i("Chat", "You are in discoverPeers()inside onPeersAvailable(WifiP2pDeviceList)." +
                                "You call onAvailableP2pDevices");

                        Chat.this.onAvailableP2pDevices(
                                peers.getDeviceList().toArray(new WifiP2pDevice[peers.getDeviceList().size()]));
                    }
                });
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.i("Chat", "You are in discoverPeers()inside WifiP2pManager.ActionListener. " +
                        "FAILURE!");
                Toast.makeText(Chat.this, "Failure: NO PEERS", Toast.LENGTH_SHORT).show();

            }
        });
    }

    /**
     * All related with server, sending and receiving messages. Storing messages in database.
     * We created to thread for the two different clients. We were not able to find the mode to
     * connect more devices. The idea would be to create a recursive method where, when a new client
     * is detected, a new thread is created. We found problems mainly with sockets.
     *
     * The messages received from one client are sent to the other client too
     */
    private void setupServer() {
        Log.i("Chat", "You are in setupServer(). Going to setupWifiDirect(boolean)");

        setupWifiDirect(true);
        //Log.i("Chat", "You are in setupServer(). Coming from setupWifiDirect(boolean). Going to discoverPeers()");


        Log.i("Chat", "You are in setupServer(). Coming from onAvailableP2pDevices. Go inside thread");

        new Thread() {
            public void run() {
                try {

                    serverSocket = new ServerSocket(PORT);
                    Log.i("Chat", "setupServer(). Inside thread. Ready to listen for clients");

                    Socket client = serverSocket.accept();
                    Log.i("Chat", "setupServer(). Inside thread. Client "+client.getInetAddress()+" connected");


                    final OutputStreamWriter out = new OutputStreamWriter(client.getOutputStream());
                    final BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    Log.i("Chat", "setupServer(). Inside thread. Output/Input buffers created ");

                    Log.i("Chat", "setupServer(). Inside thread. Server sends WELCOME CLIENT!!!");

                    out.write("Welcome client 1!!!\n");
                    out.flush();

                    new Thread() {
                        public void run() {
                            try {

                              
                                Log.i("Chat", "setupServer(). Inside thread. Ready to listen for clients");

                                Socket client = serverSocket.accept();
                                Log.i("Chat", "setupServer(). Inside thread. Client "+client.getInetAddress()+" connected");


                                final OutputStreamWriter out1 = new OutputStreamWriter(client.getOutputStream());
                                final BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                                Log.i("Chat", "setupServer(). Inside thread. Output/Input buffers created ");

                                Log.i("Chat", "setupServer(). Inside thread. Server sends WELCOME CLIENT!!!");

                                out1.write("Welcome client 2!!!\n");
                                out1.flush();



                                while(!stop) {
                                    Log.i("Chat", "setupServer(). Inside thread. Inside WHILE");

                                    line2 = in.readLine();

                                    if(messageReady){
                                        Log.i("Chat", "setupServer(). Inside thread. Server send the message if ready ");
                                        if(!sendToClient2){
                                            out1.write("Server: "+message+"\n");
                                            out1.flush();
                                            sendToClient2 = true;
                                        }
                                        counter++;
                                        if(counter ==2){
                                        messageReady = false;
                                        sendToClient1 = false;
                                        sendToClient2= false;

                                        counter =0;
                                        }
                                    }
                                    if(messageClient1Received){
                                        if(!buffer.equals("Client connected")){
                                            out1.write(buffer+"\n");
                                            out1.flush();
                                            messageClient1Received = false;
                                        }
                                    }
                                    if(line2.equals("Message that cannot be read")){
                                        line2 = null;
                                    }
                                    if(line2!=null){
                                        Log.i("Chat", "setupServer(). Server reads: "+line2);

                                        buffer2 = line2;
                                        messageClient2Received = true;
                                        //Receiving Messages
                                        runOnUiThread(new Runnable() {
                                            public void run() {
                                                Log.i("Chat", "setupServer(). Inside thread, runnable. Read and include " +
                                                        "in the list and database received messages FROM CLIENT ");

                                                Toast.makeText(getApplicationContext(), "Message received from Client", Toast.LENGTH_LONG).show();

                                                ChatMessage chatMessageServer = new ChatMessage(line2, isMine, 1, 1);
                                                list.add(chatMessageServer);
                                                listAdapter.notifyDataSetChanged();
                                                //insert message in database
                                                userMessagesDb.insertData("Client 2", line2);
                                                Log.i("Chat", "setupServer(). Inside thread. Message: "+ line2 +"read and included" +
                                                        " successfully ");


                                            }

                                        });

                                    }
                                }


                            } catch (IOException ioe) {
                                ioe.printStackTrace();
                            } catch (NullPointerException e){
                                e.printStackTrace();
                            }
                        }
                    }.start();



                    while(!stop) {
                        Log.i("Chat", "setupServer(). Inside thread. Inside WHILE");
                        //if(in.ready()){
                         line = in.readLine();

                        if(messageReady){
                            Log.i("Chat", "setupServer(). Inside thread. Server send the message if ready ");
                            if (!sendToClient1) {
                                out.write("Server: " + message + "\n");
                                out.flush();
                                sendToClient1 = true;
                            }
                            counter++;
                            if(counter ==2){
                                messageReady = false;
                                counter =0;
                                sendToClient1 = false;
                                sendToClient2 = false;
                            }
                        }
                        if(messageClient2Received){
                            if(!buffer2.equals("Client connected")){
                                out.write(buffer2 +"\n");
                                out.flush();
                                messageClient2Received = false;
                            }
                        }
                        if(line.equals("Message that cannot be read")){
                            line = null;
                        }
                        if(line!=null){
                            Log.i("Chat", "setupServer(). Server reads: "+line);

                            buffer = line;
                            messageClient1Received = true;
                            //Receiving Messages
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Log.i("Chat", "setupServer(). Inside thread, runnable. Read and include " +
                                        "in the list and database received messages FROM CLIENT ");

                                Toast.makeText(getApplicationContext(), "Message received from Client", Toast.LENGTH_LONG).show();

                                ChatMessage chatMessageServer = new ChatMessage(line, isMine, 1, 1);
                                list.add(chatMessageServer);
                                listAdapter.notifyDataSetChanged();
                                //insert message in database
                                userMessagesDb.insertData("Client 1", line);
                                Log.i("Chat", "setupServer(). Inside thread. Message: "+ line +"read and included" +
                                        " successfully ");


                            }

                        });

                        }
                    }


                } catch (IOException ioe) {
                    ioe.printStackTrace();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        }.start();


    }

    /**
     * CLIENT
     * Connect to the server. Client receives and sends messages here. Store the messages in
     * the database
     * @param device the server device
     *
     */
    private void connectToServer(WifiP2pDevice device) {
        Log.i("Chat", "connectToServer(WifiP2pDevice).Connecting to server");
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.groupOwnerIntent = 0;
        config.wps.setup = WpsInfo.PBC;
        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                mManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                    @Override
                    public void onConnectionInfoAvailable(final WifiP2pInfo wifiP2pInfo) {
                        new Thread() {
                            @Override
                            public void run() {
                                final InetAddress address = wifiP2pInfo.groupOwnerAddress;
                                mSocket = new Socket();
                                Log.i("Chat" , "connectToServer(). Connection to Server SUCCESS");


                                try {
                                    mSocket.connect((new InetSocketAddress(address, PORT)), 2000);
                                    Log.i("Chat", "connectToServer(). Sockets connected. Start reading and writing");

                                    mOutput = new OutputStreamWriter(mSocket.getOutputStream());
                                    mOutput.write("Client connected\n");
                                    mOutput.flush();
                                    mInput = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));

                                    while(!stop){
                                        Log.i("Chat", "connectToserver() WHILE");
                                        line = mInput.readLine();
                                        if(line.equals("Welcome client 1!!!")){
                                            ClientName = "Client 1: ";
                                        }

                                        if(line.equals("Welcome client 2!!!")){
                                            ClientName = "Client 2: ";
                                        }

                                        if(line!=null) {

                                             Log.i("Chat", "connectToServer(). Client reads: "+line);
                                             runOnUiThread(new Runnable() {
                                                public void run() {
                                                    Log.i("Chat", "connectToServer(). Including text in list");

                                                    Toast.makeText(getApplicationContext(), "Message received from Server", Toast.LENGTH_LONG).show();


                                                    ChatMessage chatMessageClient = new ChatMessage(line, isMine, 1, 1);
                                                    list.add(chatMessageClient);
                                                    listAdapter.notifyDataSetChanged();
                                                    userMessagesDb.insertData("Server", line);
                                                    Log.i("Chat", "connectToServer(). Inside thread. Message: "+ line +"read and included" +
                                                            " successfully ");


                                                }

                                            });



                                        }


                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }catch(NullPointerException e){
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                    }
                });
            }

            @Override
            public void onFailure(int reason) {
                //failure logic
            }
        });
    }


    /**
     * We close the sockets here
     */
    private void shutdownCommunication() {
        try {
            Log.i("Shutdown communication", "killing");
            if (mInput != null) mInput.close();
            if (mOutput != null) mOutput.close();
            if (mSocket != null) mSocket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        mIsConnected = false;
    }

    /**
     * When the app is killed, we close all sockets
     */
    protected void onDestroy() {
        super.onDestroy();
        stop = true;
        shutdownCommunication();

    }


    /**
     *
     */
    @Override
    protected void onResume() {
        super.onResume();
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
        registerReceiver(mReceiver, mIntentFilter);

    }

    /**
     *
     */
    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    /**
     * To send messages
     * @param message message we send
     */
    private void sendClientMessage(String message){
        try{
            mOutput.write(ClientName+message+"\n");
            mOutput.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     *
     * @param isWifiP2pEnabled is Wifi enable?
     */
    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;

    }



}