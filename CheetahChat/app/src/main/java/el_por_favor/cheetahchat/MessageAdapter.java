package el_por_favor.cheetahchat;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**Class Name: MessageAdapter
 *
 * Resume: All methods and variables we will use for showing messages inside a right or left cloud
 * depending on the sender
 *
 * Code taken from: http://www.devexchanges.info/2016/03/design-chat-bubble-ui-in-android.html
 *
 *
 * @author Jaime Moreno Quintanar (999132)
 * @version 4.12.2016*/
public class MessageAdapter extends ArrayAdapter<ChatMessage>{
    //Element declaration
    private Activity activity;
    List<ChatMessage> messages;

    //Constructor
    public MessageAdapter(Activity context, int resource, List<ChatMessage> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.messages = objects;
    }

    /**
     * getView. If the message is mine, it will be shown on the right side. If not, on the
     * left side
     *
     * @param position position
     * @param convertView view
     * @param parent viewgroup
     * @return convertView
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        int layoutResource = 0; // determined by view type
        ChatMessage chatMessage = getItem(position);

        //YOU ARE SERVER AND YOU ARE SENDING A MESSAGE
        if (chatMessage.isMine()) {
            layoutResource = R.layout.message_right;

        } else {
            layoutResource = R.layout.message_left;
        }

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        //set message content
        holder.msg.setText(chatMessage.getContent());

        return convertView;
    }

    /**
     * Return the number of view types we have
     * @return number of view types
     */
    @Override
    public int getViewTypeCount() {
        // return the total number of view types. this value should never change
        // at runtime
        return 2;
    }

    /**
     *
     * @param position position
     * @return position
     */
    @Override
    public int getItemViewType(int position) {
        // return a value between 0 and (getViewTypeCount - 1)
        return position % 2;
    }

    /**
     * This will be the content of every cloud
     */
    private class ViewHolder {
        private TextView msg;

        public ViewHolder(View v) {
            msg = (TextView) v.findViewById(R.id.sentMessage);
        }
    }
}
