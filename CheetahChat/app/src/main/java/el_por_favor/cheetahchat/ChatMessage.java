package el_por_favor.cheetahchat;

/**Class Name: ChatMessage
 *
 * Resume: All methods and variables we will use for showing messages inside a right or left cloud
 * depending on the sender
 *
 * Code taken from: http://www.devexchanges.info/2016/03/design-chat-bubble-ui-in-android.html
 *
 *
 * @author Jaime Moreno Quintanar (999132)
 * @version 4.12.2016*/

public class ChatMessage {
    private String content;
    private boolean isMine;
    private int sender; //1 SERVER       2 CLIENT
    private int receiver;//1 SERVER       2 CLIENT


    //Constructor
    public ChatMessage(String content, boolean isMine, int sender, int receiver) {
        this.content = content;
        this.isMine = isMine;
        this.sender = sender;
        this.receiver = receiver;
    }

    //Getters and setters

    public String getContent() {
        return content;
    }


    public boolean isMine() {
        return isMine;
    }

    public int getSender() {
        return sender;
    }

    public int getReceiver() {
        return receiver;
    }

    public void setContent(String content){this.content = content;}

    public void setIsMine(boolean isMine){this.isMine = isMine;}

    public void setReceiver(int receiver) {
        this.receiver = receiver;
    }

    public void setSender(int sender) {
        this.sender = sender;
    }
}
