package el_por_favor.cheetahchat;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.widget.Toast;

/**
 * Responsible for managing WiFi P2P events.
 *
 * This is a skeleton implementation of the P2P subsystem,
 * that reacts to system events around WiFi direct.
 * More information
 * <a href="https://developer.android.com/training/connect-devices-wirelessly/wifi-direct.html">
 *     on Google Developers page</a>.
 *
 * This class is taken from the app SmartColor that Mariusz published on Fronter
 * @author Andres Escalante
 * @version 1.12.2016
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private Chat mActivity;
    private MainMenu mActivity1;
    private boolean isWifiP2pEnabled = false;


    WifiP2pManager.PeerListListener myPeerListListener;


    /**
     * Constructor 1
     * Broadcast receiver for WiFi P2P notifications.
     *
     * @param mManager wifip2pmanager
     * @param mChannel channel
     * @param mActivity activity
     */
    public WiFiDirectBroadcastReceiver(final WifiP2pManager mManager,
                                       final WifiP2pManager.Channel mChannel,
                                       final Chat mActivity) {
        super();
        this.mManager = mManager;
        this.mChannel = mChannel;
        this.mActivity = mActivity;
    }

    /**
     * Constructor 2
     * @param mManager wifip2pmanager
     * @param mChannel channel
     * @param mActivity1 activity
     */
    public WiFiDirectBroadcastReceiver(final WifiP2pManager mManager,
                                       final WifiP2pManager.Channel mChannel,
                                       final MainMenu mActivity1) {
        super();
        this.mManager = mManager;
        this.mChannel = mChannel;
        this.mActivity1 = mActivity1;
    }


    /**
     * Is WifiP2P enabled?
     * @param context context
     * @param intent intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);

            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                mActivity.setIsWifiP2pEnabled(true);
                Toast.makeText(this.mActivity, "WiFi P2P IS enabled.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this.mActivity, "WiFi P2P is NOT enabled.", Toast.LENGTH_SHORT).show();
                mActivity.setIsWifiP2pEnabled(false);


            }

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // Ignore
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            // TODO Respond to new connection or disconnections
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // TODO Respond to this device's wifi state changing
        }

    }

}
