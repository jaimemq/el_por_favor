Cheetah Chat is a messaging application, which does not require an Internet access or mobile network.
It is based on mesh networking and P2P connection between Android devices.
Wi-Fi Direct protocol makes it possible to have a direct live conversation 
within a considerable distance without any costs or need for additional 
devices like router or modem. It has perfect use in facilities like hospitals 
where a network infrastructure may not be of the best quality. 
Cheetah Chat is the best solution in the areas lacking mobile signal and proper 
Wi-Fi connectivity. It is free to use and power efficient making it possible to chat for a long time. 
The application also guarantees the highest level of privacy, because of its peer-to-peer connectivity. 
Only the two parties have access to the exchanged data.

Group Members: Bartłomiej Morawiec, David Eguizábal Pérez, Andrés Escalante Ariza, Jaime Moreno Quintanar 
Group Name: Cheetah Chat
Repository: https://jaimemq@bitbucket.org/jaimemq/el_por_favor.git 
Course: Mobile Development Project (IMT3672)